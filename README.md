# Playground for pgBackRest passwordless SSH setup

This repository provides a Docker compose setup for testing and playing with
Passwordless SSH communication. It aims at illustrating how things work
(the *servers* and *clients* in particular), which part of the configuration
is useful for why, and it tries to decouple things as much as possible in order
to avoid the bias of having everything set up on the same machine.

The setup consists of 3 types of services:

1. several Postgres database servers (`pg15`, `pg14`), configured for
   archiving with pgbackrest, each using their own stanza
2. a pgBackRest `repository` running a ssh server
3. a `ssh` server running alongside (but outside) the Postgres
   services with access to `$PGDATA`; this service (process) is *common* to all
   database servers

Communication happens as follows:

* WAL archiving goes from the `pg*` host to the `repository` (through
  `archive_command = pgbackrest archive-push`)
* backups are run from `repository`, thus pulling data from the `pgbackrest`
  service (not Postgres hosts) with client command `pgbackrest backup`
* `pgbackrest restore` (client) commands, emitted from the `pg*` host to
  the `repository`

## pgBackRest (and PostgreSQL) configuration

### Database hosts

Postgres nodes only run client `pgbackrest` commands (typically `archive-push`
and possibly `restore`). WAL archiving is configured normally as:

    archive_mode = on
    archive_command = 'pgbackrest --stanza=<stanza> archive-push %p'

And the pgBackRest configuration consists in:

*   A `[global]` section:

    ```ini
    [global]
    repo1-host=repository
    repo1-host-config=/srv/pgbackrest/pgbackrest.conf
    repo1-host-type=ssh
    ```

    The remote repository is referenced there; notice the `repo1-host-config`
    item which declares the location of the configuration file *on the remote
    host*, when using a non-default value.

    The `repo1-host-type=ssh` is not really required since its the default value.

*   a `[<STANZA>]` section with `pg1-path` and `pg1-port` (if needed) defined.

In order to run `pgbackrest` client commands, the stanza needs to be specified;
typically, we export a `PGBACKREST_STANZA` environment variable for that.

### The repository host

On this repository host, we install a *client* configuration:

```ini
[global]
repo1-path=/var/lib/backups
start-fast=y

[fst]
pg1-host=pg15
pg1-path=/var/lib/postgresql/15/main

[snd]
pg1-host=pg14
pg1-path=/var/lib/postgresql/14/main
pg1-port=5433
```

This configuration is typically used by `pgbackrest backup` command.

## Build, run and play

First create the ssh keys, build Docker images, start all services:

```console
$ make keys
$ docker-compose build
$ docker-compose up -d
```

Then exchange SSH keys and check that repository and pgbackrest servers can
communicate via SSH for their respective users.

```console
$ make check-ssh
```
You should actually see `pgbackrest` help message if everything goes well.

Create the stanzas (on the repository host):

```console
$ make stanzas
```

Check configuration (on repository and database hosts):

```console
$ make verify
$ make check
```

Run a pgbench, to see if archiving works:

```console
$ make bench
```

## Backup

Creating a backup is done on the repository host, e.g. for stanza `fst`:

```console
$ make backup
```

## Restore

In order to test restore, let's first simulate a damage to Postgres data:

```console
$ make damage
```

Then request a `restore` *from* this Postgres host (by running an ephemeral
container with `$PGDATA` etc.):

```console
$ make restore
```

## References

* <https://pgbackrest.org/user-guide.html#repo-host/setup-ssh>
* <https://gitlab.com/dlax/pgbackrest-tls-repository-playground>

<!--
   vim: spelllang=en spell
   -->
