keys:
	$(MAKE) -C .keys
	cp .keys/id_rsa_pg* postgres/keys/.
	cp .keys/id_rsa_repository repository/keys/id_rsa
	cp .keys/id_rsa_repository.pub repository/keys/id_rsa.pub
	# Exchange keys from repository to sshd
	(echo -n 'no-agent-forwarding,no-X11-forwarding,no-port-forwarding,' && \
	 echo -n 'command="/usr/bin/pgbackrest $${SSH_ORIGINAL_COMMAND#* }" ' && \
	 cat repository/keys/id_rsa.pub) | tee -a sshd/keys/authorized_keys
	# Exchange keys from pg15 to repository
	(echo -n 'no-agent-forwarding,no-X11-forwarding,no-port-forwarding,' && \
	 echo -n 'command="/usr/bin/pgbackrest $${SSH_ORIGINAL_COMMAND#* }" ' && \
	 cat postgres/keys/id_rsa_pg15.pub) | tee -a repository/keys/authorized_keys
	# Exchange keys from pg14 to repository
	(echo -n 'no-agent-forwarding,no-X11-forwarding,no-port-forwarding,' && \
	 echo -n 'command="/usr/bin/pgbackrest $${SSH_ORIGINAL_COMMAND#* }" ' && \
	 cat postgres/keys/id_rsa_pg14.pub) | tee -a repository/keys/authorized_keys

check-ssh:
	docker-compose exec repository ssh postgres@sshd -p 22345
	docker-compose exec pg15 ssh pgbackrest@repository -p 22456
	docker-compose exec pg14 ssh pgbackrest@repository -p 22456

stanzas:
	docker-compose exec repository pgbackrest stanza-create --stanza=fst
	docker-compose exec repository pgbackrest stanza-create --stanza=snd

verify:
	docker-compose exec pg15 pgbackrest verify
	docker-compose exec pg14 pgbackrest verify
	docker-compose exec repository pgbackrest verify --stanza=fst
	docker-compose exec repository pgbackrest verify --stanza=snd

check:
	docker-compose exec pg15 pgbackrest check
	docker-compose exec pg14 pgbackrest check
	docker-compose exec repository pgbackrest check --stanza=fst --no-archive-check --archive-timeout=5
	docker-compose exec repository pgbackrest check --stanza=snd --no-archive-check --archive-timeout=5

bench:
	docker-compose exec pg15 createdb bench
	docker-compose exec pg15 pgbench -i bench

backup:
	docker-compose exec repository pgbackrest --stanza=fst backup
	docker-compose exec repository pgbackrest --stanza=snd backup

damage:
	docker-compose exec pg15 rm /var/lib/postgresql/15/main/global/pg_control
	docker-compose exec pg14 rm /var/lib/postgresql/14/main/global/pg_control
	docker-compose restart pg15 pg14

restore:
	docker-compose run pg15 pgbackrest restore --delta
	docker-compose run pg14 pgbackrest restore --delta
	docker-compose restart pg15 pg14

clean:
	$(MAKE) -C .keys clean
	-rm repository/keys/* sshd/keys/* postgres/keys/*
